<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjetRepository")
 */
class Projet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="projet")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userProjet;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Selection", mappedBy="projet")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUserProjet(): ?User
    {
        return $this->userProjet;
    }

    public function setUserProjet(?User $userProjet): self
    {
        $this->userProjet = $userProjet;

        return $this;
    }

    /**
     * @return Collection|Selection[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(Selection $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setProjet($this);
        }

        return $this;
    }

    public function removeUser(Selection $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getProjet() === $this) {
                $user->setProjet(null);
            }
        }

        return $this;
    }
}
