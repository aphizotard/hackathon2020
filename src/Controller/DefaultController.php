<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function home()
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user, ['action'=>$this->generateUrl('app_register')]);
        return $this->render('index.html.twig', [
            'form'=> $form->createView()
        ]);
//        return new Response("Test premiere page");
    }

    /**
     * @Route("/office", name="back_office")
     */
    public function office()
    {
        return $this->render('back_index.html.twig');
    }

    /**
     * @Route("/elearning", name="elearning")
     */
    public function learning()
    {
        return $this->render('elearning.html.twig');
    }
}