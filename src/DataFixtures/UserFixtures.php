<?php

namespace App\DataFixtures;

use App\Entity\Projet;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            $user = (new User())
                ->setLastname($faker->lastName)
                ->setFirstname($faker->firstName)
                ->setEmail($faker->email)
                ->setPassword('string')
                ->setChoix($faker->randomElement(User::CHOIX));


            $manager->persist($user);


         {
//            $currentUser = $faker->randomElement($user);

            $projet = (new Projet())
                ->setTitre($faker->sentence(5))
                ->setUserProjet($user)
                ->setDescription($faker->sentence(15))
            ;

            $manager->persist($projet);
        }
        }

        $manager->flush();
    }
}
