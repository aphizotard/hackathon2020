<?php

namespace App\DataFixtures;

use App\Entity\Projet;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $users = $manager->getRepository(User::class)->findAll();

        for ($i = 0; $i < 25; $i++) {

            $projet = (new Projet())
                ->setTitre($faker->word)
                ->setDescription($faker->text)
                ->setUserProjet($faker->randomElement($users));


            $manager->persist($projet);
        }

        $manager->flush();
    }
}
