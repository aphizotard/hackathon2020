## Hackathon 2020 - ADW

    * Installation du projet: 
        * composer install
        * docker-compose build
        * docker-compose up -d

    * Ports
        * App: localhost:8060
        * Bdd: localhost:8062
        
    * Commandes:
        Préfixer les commandes par : docker-compose exec php bin/console

        - doctrine:database:create : créer la bdd
        - doctrine:schema:update --dump-sql : mettre à jour la bdd
        - doctrine:schema:update --force : mettre à jour la bdd




    